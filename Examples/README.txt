The sub-directories of the Examples\ directory contain examples that should contain sufficient data to be run directly with the current version of Aurora. The only exception is Precipitation_Source_Multiple_Wells\, which is for testing an upcoming version of Aurora and will not run with the distrbuted version of the software!

In all cases (save for the previously mentioned exception) you should be able to produce output by running Aurora, specifying the sub-directory (e.g., Push_Pull\) and the name of a marshal file within it (these ater *.txt files that typically contain 'Marshal' or 'marshal' in their filenames). For size reasons, output files are generally not included in the respository or the distributed archive. 

Once you run Aurora to produce output, this output can be explored with the Python scripts in the ..\Visualizer directory. These scripts currently employ hard-coded paths, so you may have to change them to point the sub-directory you ran Aurora on before running the scripts.

In most cases, the example subdirectories contain a *.gpt file, which is the USGS ModelMuse file used to create the *.dis input and output files (*.cbc and/or *.bhd raw binary files). You can open this with ModelMuse to explore the model in detail. In some older examples, the *.cbc files have been pre-processed by GW Chart into *.f* human-readable files: these may appear instread of the *.cbc files or in addition to them.

Not all the examples are currently well documented. Two good examples to start with:

1) Simple_Inclusion\ : contains a simuation in a simple domain that contains an irregularly-shaped low-permeability feature. The directory contains a *.gpt file, as well as an image (flow_model_streamines.png) which shows domain geometry and pathlines through the domain. Modifying the marshal file to change the physics in the DOMAIN block is a good way to get a feel for how different physics alter breakthrough behaviour. A number of alternative marshal files are included, displaying both the *.f* and *.cbc methods for specifying MODFLOW water budgets, as well as different source types.

2) Push_Pull\ : contains a simulation of a single-well injection/withdrawal test. This contains a *.gpt illustrating the model and marshal files displaying both the *.f* and *.cbc methods for specifying output. Unusually, it also contains model output and visualizations (in sub-directories), which gives an idea of visualization capabilities and output formats.


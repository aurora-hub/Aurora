MAIN
    tce_decay.dis      	MODFLOW geometry discretization file name
    ASSUME_SATURATED
    GW_CHART_FILES
        tce_decay.f2        GW-Chart right flux file name
        NIL 		        GW-Chart front flux file name
        NIL                 GW-Chart bottom flux file name
    ESB
    1.0					Spatial length of each step along streamline [L]
    2e8 				Maximum time of simulation [T]
    AUTO_GRID_OFFSET
    MOLES_PER_PARTICLE -> 1e-6
END

DOMAIN
    0.25				Assumed porosity
    TRANSVERSE_DISP -> 2.5 0.25
    ADE -> 25           
    EXPONENTIAL -> 4e-4 5e-4
END

SPECIES
    TCE
    DCE
    VC
END

DECAY
    TCE -> 1.456e-8 DCE
    DCE -> 1.68e-8 [VC 1]
    VC -> 1.36e-8 Null
END

MIMT_ADJUSTMENT
    TCE -> 4  1
    VC -> 1.5 1
END

MOLAR_SOURCE
    FIXED_CONC
        [0.0  1e-7]     Concentration
    ESB
    END_TIME -> 2e7 
    BOX
        5   			Release box: Minimum X [L]
        6			    Release box: Maximum X [L]
        -100			Release box: Minimum Y [L]
        0			    Release box: Maximum Y [L]
        -10			    Release box: Minimum Z [L]
        0			    Release box: Maximum Z [L]
    ESB
    SPECIES -> TCE
END

BREAKTHROUGHS
    PLANE -> 1 0 0 495 EITHER
END

PROFILES			Times at which particle snapshots are desired
    5e6
    5e7
    1.0e8
    2.0e8
END
﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using MathNet.Numerics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
    class ArbitrarySampler : ILocationSampler
    {
        public delegate Vector<double> Proposal(Vector<double> OldVector);
        public delegate double ProbabilityMass(Vector<double> Location);
        public double Centroid;
        public bool Rescaled;

        private Vector<double> StateVector;
        private List<Vector<double>> SampleList;
        private double StateMass;
        private readonly Proposal MyProposal;
        private readonly ProbabilityMass MyProbabilityMass;

        public ArbitrarySampler(Vector<double> SeedVector, Proposal Proposal, ProbabilityMass ProbabilityMass)
        {
            this.StateVector = SeedVector;
            this.MyProposal = Proposal;
            this.MyProbabilityMass = ProbabilityMass;
            this.StateMass = this.MyProbabilityMass(this.StateVector);
            this.Rescaled = false;
            const int NumSamples = 5000;
            const double Threshold = 0.05;

            /*
             * Maintain slowly-updating rolling averages and quickly updating rolling averages of mean and variance
             * If these converge, the chain is burned in. Store the computed mean value and use for the normalization
             * of the chain, if needed.
             */
            SampleList = new();
            double Mean = 0, Var = 0, LastMean, LastVar;
            do //Add samples to the bank
            {
                LastMean = Mean;
                LastVar = Var;
                var NewSamples = GenerateSamples(NumSamples);
                SampleList.AddRange(NewSamples);
                (Mean, Var) = ComputeStatistics(SampleList);
            } while (Math.Abs(Mean - LastMean) / Mean > Threshold || Math.Abs(Var - LastVar) / Var > Threshold);
            this.Centroid = Mean;
            Debug.WriteLine("Sample generation complete. Samples: " + SampleList.Count.ToString());
        }

        private List<Vector<double>> GenerateSamples(int NumSamples)
        {
            var SampleList = new List<Vector<double>>();
            for (int i = 0; i < NumSamples; i++)
                SampleList.Add(RawSample());
            return SampleList;
        }

        private (double , double) ComputeStatistics(List<Vector<double>> SampleList)
        {
            int NumItems = 0;
            double TotalSum = 0, TotalSquare = 0;
            foreach(var Sample in SampleList)
            {
                NumItems++;
                var Magnitude = Sample.L2Norm();
                TotalSum += Magnitude;
                TotalSquare += Magnitude * Magnitude;
            }
            var Mean = TotalSum / NumItems;
            var Var = TotalSquare / NumItems - Mean * Mean;
            return (Mean, Math.Sqrt(Var));
        }
        

        public Vector<double> RawSample()
        {
            Vector<double> CandidateVector;
            double CandidateMass, AcceptanceRatio;

            CandidateVector = MyProposal(StateVector);
            CandidateMass = MyProbabilityMass(CandidateVector);
            AcceptanceRatio = CandidateMass / StateMass;

            if (ContinuousUniform.Sample(0, 1) < AcceptanceRatio) /// changed to < (before:>)
            {
                StateVector = CandidateVector;
                StateMass = CandidateMass;
            }
            return (StateVector);
        }

        public Vector<double> Sample()
        {
            var Idx = (int) Math.Floor(SampleList.Count * Generate.Uniform(1)[0]);
            return Rescaled ? SampleList[Idx] / Centroid : SampleList[Idx];
        }
    }
}

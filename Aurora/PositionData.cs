﻿using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
	struct PositionData
	{
		public int XCellIndex;
		public int YCellIndex;
		public int ZCellIndex;
		public double XFraction;
		public double YFraction;
		public double? ZFraction;
		public Vector<double>? Excess;
	}
}
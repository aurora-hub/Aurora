﻿using System;
using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
    enum TransitionStatus
    {
        Enter,
        Exit,
        Decayed,
        EpochChange,
        NoTransition
    }

    class SourceEvent
    {
        public MolarConcentrationSource Source;
        private readonly string Species;

        public SourceEvent(MolarConcentrationSource Source, string Species)
        {
            this.Source = Source;
            this.Species = Species;
        }

        private Vector<double>? Interpolate(Vector<double> StartLocation, Vector<double> EndLocation, int Depth = 6)
        {
            bool StartIn = Source.SourceRegion.IsInside(StartLocation);
            bool EndIn = Source.SourceRegion.IsInside(EndLocation);
            if (StartIn == EndIn) 
                return null;
            else 
            {
                var MidLocation = (StartLocation + EndLocation) / 2;
                if (Depth > 0)
                {
                    bool MidIn = Source.SourceRegion.IsInside(MidLocation);
                    if (StartIn == MidIn)
                        return Interpolate(MidLocation, EndLocation, Depth - 1);
                    else
                        return Interpolate(StartLocation, MidLocation, Depth - 1);
                }
                else
                {
                    return MidLocation;
                }
            }
        }

        private double InterpolationFraction(Vector<double> StartLocation, Vector<double> EndLocation)
        {
            var TransitionLocation = Interpolate(StartLocation, EndLocation);
            return (TransitionLocation - StartLocation).L2Norm() / (EndLocation - StartLocation).L2Norm();
        }

        public (TransitionStatus, double) CheckTransition(Vector<double> OldLocation, Vector<double> NewLocation, double OldTime, double NewTime, double DecayDelta, string Species)
        {
            if (Species != this.Species || OldTime > Source.EndTime || NewTime < Source.EpochStartTimes[0])
                return (TransitionStatus.NoTransition, -1);

            bool OldIn = Source.SourceRegion.IsInside(OldLocation);
            bool NewIn = Source.SourceRegion.IsInside(NewLocation);
            
            int OldEpochIdx = Source.GetEpoch(OldTime);
            int NewEpochIdx = Source.GetEpoch(NewTime);

            //If there is a change in epoch, this is when it is, otherwise not relevant
            double EpochChangeTime = Source.EpochStartTimes[Math.Min(OldEpochIdx+1,NewEpochIdx)];

            if (OldIn && !NewIn) //Particle has departed
            {
                var Fraction = InterpolationFraction(OldLocation, NewLocation);
                var ExitTime = (1 - Fraction) * OldTime + Fraction * NewTime;

                if (OldEpochIdx != NewEpochIdx && EpochChangeTime < ExitTime)
                    return (TransitionStatus.EpochChange, EpochChangeTime);
                else
                    return (TransitionStatus.Exit, ExitTime);
            }
            else if (!OldIn && NewIn) //Particle has entered
            {
                return (TransitionStatus.Enter, -1);
            } 
            else if (OldIn && NewIn) //Particle remains inside...but does it DIE???
            {
                if (OldEpochIdx != NewEpochIdx)
                    return (TransitionStatus.EpochChange, EpochChangeTime);
                if (OldTime + DecayDelta <= NewTime)
                    return (TransitionStatus.Decayed, OldTime + DecayDelta);
            }
            //Nothing has happened if flow makes it to this point
            return (TransitionStatus.NoTransition, -1);
        }
    }
}
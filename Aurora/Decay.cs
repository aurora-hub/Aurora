﻿using System;
using System.Collections.Generic;

namespace Aurora
{
	public readonly struct DecayRule
	{

		public readonly double Rate;
		public readonly string[] Destinations;
		public readonly int[] DestinationMoles;

		public DecayRule(double Rate, string[] Destinations, int[] DestinationMoles)
		{
			this.Rate = Rate;
			this.Destinations = Destinations;
			this.DestinationMoles = DestinationMoles;
		}
	}

	class DecayRules : List<DecayRule>
	{
		public DecayRules() : base() { }
	}
}
﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using System;

namespace Aurora
{
    class TPLSampler
    {
        /*
        [DllImport("TPLDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern double tpowdev(double beta, double tau1, double tau2);
        
        public static double Sample(double TPLBeta, double TPLT1, double TPLT2)
        {
            double temp = tpowdev(TPLBeta, TPLT1, TPLT2);
            return temp;
        }
        */
        private readonly double T1, T2, Beta, Shifter;
        private readonly ArbitrarySampler Sampler;

        public TPLSampler(double T2T1Ratio, double Beta, bool Shifted)
        {
            this.T1 = 1;
            this.T2 = T2T1Ratio;
            this.Beta = Beta;
            
            Shifter = Shifted ? T1 : 0;    

            Sampler = new ArbitrarySampler(Vector<double>.Build.Dense(1), 
                new ArbitrarySampler.Proposal(Proposal), new ArbitrarySampler.ProbabilityMass(Mass));

            //Re-centre the sampler so that it has unit mean; proposal changed after burn-in.
            Sampler.Rescaled = true;
            
            T1 *= Sampler.Centroid;
            T2 *= Sampler.Centroid;
        }

        public TPLSampler(double T1, double T2, double Beta, bool Shifted)
        {
            this.T1 = T1;
            this.T2 = T2;
            this.Beta = Beta;

            Shifter = Shifted ? T1 : 0;

            Sampler = new ArbitrarySampler(Vector<double>.Build.Dense(1),
                new ArbitrarySampler.Proposal(Proposal), new ArbitrarySampler.ProbabilityMass(Mass));
        }

        private Vector<double> Proposal(Vector<double> LastVector)
        {
            //Build a heavy-tailed symmetric sampler that is operative on the positive real axis 
            var Delta = Pareto.Sample(T1, Beta) - T1;
            Delta *= Math.Sign(ContinuousUniform.Sample(-1, 1));
            var Candidate = Math.Abs(LastVector[0] + Delta);

            return Vector<double>.Build.Dense(new double[1] { Candidate });
        }

        private double Mass(Vector<double> Location)
        {
            double T = Location[0];
            return Math.Pow((T + Shifter) / T1, -Beta) * Math.Exp(-T / T2);
        }

        public double Sample()
        {
            return Sampler.Sample()[0];
        }
    }
}

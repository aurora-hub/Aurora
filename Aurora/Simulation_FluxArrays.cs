﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
    partial class Simulation {

        double[,,,] FlowFrontFace;
        double[,,,] FlowRightFace;
        double[,,,] FlowLowerFace;
        
        public void CellByCellFluxArray(String CellByCellFilePath)  
        {
            int RealValueBytes = 4;
            int DoubleCheck = 0;

            using (FileStream FileStreamForSingle = new FileStream(CellByCellFilePath, FileMode.Open, FileAccess.Read))
                ReadCBCFile(out DoubleCheck, out FlowFrontFace, out FlowRightFace, out FlowLowerFace, FileStreamForSingle, RealValueBytes, out FluxArrayIndexToStepIndex);
            if (DoubleCheck == 1)
            {
                RealValueBytes = 8;
                
                using (FileStream FlieStreamForDouble = new FileStream(CellByCellFilePath, FileMode.Open, FileAccess.Read))
                    ReadCBCFile(out DoubleCheck, out FlowFrontFace, out FlowRightFace, out FlowLowerFace, FlieStreamForDouble, RealValueBytes, out FluxArrayIndexToStepIndex);
            }

            XPlusFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ, FluxArrayIndexToStepIndex.Count];
            YMinusFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ, FluxArrayIndexToStepIndex.Count];
            ZFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ + 1, FluxArrayIndexToStepIndex.Count];
            double[,,,] RechargeRateBuffer = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];

            for (int FluxArrayIdx = 0; FluxArrayIdx < FluxArrayIndexToStepIndex.Count; FluxArrayIdx++)
            {
                for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                {
                    for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                    {
                        for (int ZCellIndex = 0; ZCellIndex < NumCellsZ; ZCellIndex++)
                        {
                            //Need to FLIP YCellIndex: last row in file should correspond to YCellIndex = 0
                            //Need to FLIP ZCellIndex: last row in file should correspond to ZCellIndex = 0
                            XPlusFluxArray[XCellIndex, YCellIndex, ZCellIndex, FluxArrayIdx] =
                            FlowRightFace[XCellIndex, NumCellsY - YCellIndex - 1, NumCellsZ - ZCellIndex - 1, FluxArrayIdx];
                            YMinusFluxArray[XCellIndex, YCellIndex, ZCellIndex, FluxArrayIdx] =
                            -1 * FlowFrontFace[XCellIndex, NumCellsY - YCellIndex - 1, NumCellsZ - ZCellIndex - 1, FluxArrayIdx];
                            ZFluxArray[XCellIndex, YCellIndex, ZCellIndex, FluxArrayIdx] =
                            -1 * FlowLowerFace[XCellIndex, YCellIndex, NumCellsZ - ZCellIndex - 1, FluxArrayIdx];

                            RechargeRateBuffer[XCellIndex, YCellIndex, ZCellIndex, FluxArrayIdx] 
                                = RechargeRate[XCellIndex, NumCellsY - YCellIndex - 1, NumCellsZ - ZCellIndex - 1, FluxArrayIdx];
                        }
                    }
                }
            }
            RechargeRate = RechargeRateBuffer;
            //CreateArrayFile(RechargeRate, "Recharge_rate_array");
        }

        public static double ReadReal(FileStream FileStream, int RealValueBytes)
        {
            BinaryReader BinaryReader = new BinaryReader(FileStream);

            byte[] BytesData = BinaryReader.ReadBytes(RealValueBytes);
            double DoubleData = 0;

            if (RealValueBytes == 4)
            {
                float SingleData = BitConverter.ToSingle(BytesData, 0);
                DoubleData = Convert.ToDouble(SingleData);

            }
            else if (RealValueBytes == 8)
                DoubleData = BitConverter.ToDouble(BytesData, 0);
            else
                Console.WriteLine("[Warning] Errors in reading real values");

            return DoubleData;
        }

        /*
         * Allocates big enough buffers in FlowFrontFace, etc. to store data for each time step, but only stores where 
         * there is data (potentially leaves some step layers empty).
         */
        public void ReadCBCFile(out int DoubleCheck, out double[,,,] FlowFrontFace, out double[,,,] FlowRightFace,
            out double[,,,] FlowLowerFace, FileStream FileStream, int RealValueBytes, out List<int> FluxArrayIndexToStepIndex)
        {
            FlowFrontFace = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];
            FlowRightFace = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];
            FlowLowerFace = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];

            FluxArrayIndexToStepIndex = new List<int>();

            WellFluxes = new List<WellData>();
            RechargeRate = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];

            List<string> Contents = new List<string>();
            List<string> ContentsForFirstTwoLoops = new List<string>();
            ContentsForFirstTwoLoops.Add("STORAGE");
            ContentsForFirstTwoLoops.Add("CONSTANT_HEAD");
            ContentsForFirstTwoLoops.Add("FLOW_RIGHT_FACE");
            ContentsForFirstTwoLoops.Add("FLOW_FRONT_FACE");
            ContentsForFirstTwoLoops.Add("FLOW_LOWER_FACE");

            using (BinaryReader BinaryReader = new BinaryReader(FileStream))
            {
                int NextChar;
                DoubleCheck = 0;  //default: Real-value = Single
                int LoopCount = 1;
                int CurrentTimeStep = 1;

                do
                {
                    var Step = BinaryReader.ReadInt32(); //KSTP: the current time step number
                    var Period = BinaryReader.ReadInt32(); //KPER: the current stress period number
                    //Debug.Print("Step " + Step.ToString() + ", Period " + Period.ToString());

                    int StepIdx = Period > 1? StressPeriodCumulativeSteps[Period - 2] + Step - 1: Step - 1;
                    if (FluxArrayIndexToStepIndex.Count == 0)
                        FluxArrayIndexToStepIndex.Add(StepIdx);
                    else if (FluxArrayIndexToStepIndex[^1] < StepIdx)
                        FluxArrayIndexToStepIndex.Add(StepIdx);

                    string FormattedDESC = new string(BinaryReader.ReadChars(16)).Trim();
                    FormattedDESC = FormattedDESC.Replace(" ", "_");
                    if (!Contents.Contains(FormattedDESC))
                        Contents.Add(FormattedDESC);
                    if (LoopCount != 1 && Contents[0] == FormattedDESC)
                        CurrentTimeStep++;

                    BinaryReader.ReadInt32(); //for NCOL
                    BinaryReader.ReadInt32(); //for NROW
                    int NLAY = BinaryReader.ReadInt32();

                    if (LoopCount == 2 && !ContentsForFirstTwoLoops.Contains(FormattedDESC)) //check if the data type is double precision
                    {
                        DoubleCheck = 1;
                        break;
                    }

                    if (NLAY > 0)
                    {
                        for (int i = 0; i < NumCellsZ; i++)
                        {
                            for (int j = 0; j < NumCellsY; j++)
                            {
                                for (int k = 0; k < NumCellsX; k++)
                                {
                                    if (FormattedDESC == "FLOW_RIGHT_FACE")
                                        FlowRightFace[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "FLOW_FRONT_FACE")
                                        FlowFrontFace[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "FLOW_LOWER_FACE")
                                        FlowLowerFace[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    /*else if (FormattedDESC == "RECHARGE")
                                        RechargeRate[k,j,i,CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "WELLS")
                                    {
                                        if (WellFluxes.Exists(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { k, NumCellsY - j - 1, NumCellsZ - i - 1 }))))
                                            WellFluxes[WellFluxes.FindLastIndex(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { k, NumCellsY - j - 1, NumCellsZ - i - 1 })))].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        else
                                        {
                                            WellFluxes.Add(new WellData
                                            {
                                                CellIndexes = Vector<double>.Build.Dense(new double[3] { k, NumCellsY - j - 1, NumCellsZ - i - 1 }), //Flipping Y and Z index
                                                FlowRates = new double[NumTimeSteps]
                                            });
                                            WellFluxes[WellFluxes.Count - 1].FlowRates[CurrentTimeStep-1] = ReadReal(FileStream, RealValueBytes);
                                        }
                                    }*/
                                    else
                                        ReadReal(FileStream, RealValueBytes);
                                }
                            }
                        }
                    }
                    else if (NLAY < 0)
                    {
                        int ITYPE = BinaryReader.ReadInt32();
                        ReadReal(FileStream, RealValueBytes); //DELT: the length of the current time step
                        ReadReal(FileStream, RealValueBytes); //PERTIM: the time in the current stress period
                        ReadReal(FileStream, RealValueBytes); //TOTIM: the total elapsed time
                        int NVAL = 1; // NVAL: the number of values associated with each cell (Assumption: Integer)
                        if (ITYPE == 5)
                        {
                            NVAL = BinaryReader.ReadInt32();

                            if (NVAL > 1)
                            {
                                for (int i = 1; i < NVAL; i++)
                                    BinaryReader.ReadChars(16); //CMTP: a description of the additional value associated with each cell
                            }
                        }
                        else
                            NVAL = 1;

                        //* Now read the data for each cells based on ITYPE *//

                        if (ITYPE == 0 || ITYPE == 1)
                        {
                            for (int i = 0; i < NumCellsZ; i++)
                            {
                                for (int j = 0; j < NumCellsY; j++)
                                {
                                    for (int k = 0; k < NumCellsX; k++)
                                    {
                                        if (FormattedDESC == "FLOW_RIGHT_FACE")
                                            FlowRightFace[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        else if (FormattedDESC == "FLOW_FRONT_FACE")
                                            FlowFrontFace[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        else if (FormattedDESC == "FLOW_LOWER_FACE")
                                            FlowLowerFace[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        /*else if (FormattedDESC == "RECHARGE")
                                            RechargeRate[k, j, i, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        else if (FormattedDESC == "WELLS")
                                        {
                                            if (WellFluxes.Exists(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { k, NumCellsY - j - 1, NumCellsZ - i - 1 }))))
                                                WellFluxes[WellFluxes.FindLastIndex(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { k, NumCellsY - j - 1, NumCellsZ - i - 1 })))].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                            else
                                            {
                                                WellFluxes.Add(new WellData
                                                {
                                                    CellIndexes = Vector<double>.Build.Dense(new double[3] { k, NumCellsY - j - 1, NumCellsZ - i - 1 }), //Flipping Y and Z index
                                                    FlowRates = new double[NumTimeSteps]
                                                });
                                                WellFluxes[WellFluxes.Count - 1].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                            }
                                        }*/
                                        else
                                            ReadReal(FileStream, RealValueBytes);
                                    }
                                }
                            }
                        }
                        else if (ITYPE == 2 || ITYPE == 5)
                        {
                            int NLIST = BinaryReader.ReadInt32(); //NLIST: the number of cells for which values will be stored.

                            if (NLIST > 0)
                            {
                                int NRC = NumCellsX * NumCellsY;
                                int ICELL = 0; //ICELL: cell index
                                double DoubleNVAL = 0;

                                for (int i = 0; i < NLIST; i++)
                                {
                                    ICELL = BinaryReader.ReadInt32();
                                    while (ICELL < 1)
                                        ICELL = BinaryReader.ReadInt32();
                                    DoubleNVAL = ReadReal(FileStream, RealValueBytes);

                                    int Layer = (int)MathF.Floor(((ICELL - 1) / NRC + 1));
                                    int Row = (int)MathF.Floor(((ICELL - (Layer - 1) * NRC) - 1) / NumCellsX + 1);
                                    int Column = (int)MathF.Floor(ICELL - (Layer - 1) * NRC - (Row - 1) * NumCellsX);

                                    if (Layer < 1 || Layer > NumCellsZ || Row < 1 || Row > NumCellsY || Column < 1 || Column > NumCellsX)
                                    {
                                        DoubleCheck = 1;
                                        Console.WriteLine("The data is written in Double precision");
                                        break;
                                    }

                                    if (FormattedDESC == "FLOW_RIGHT_FACE")
                                    {
                                        if (FlowRightFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] != 0)   // The case where more than 2 wells locate in one cell is considered.
                                            DoubleNVAL = FlowRightFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] + DoubleNVAL;
                                        FlowRightFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] = DoubleNVAL;
                                    }
                                    else if (FormattedDESC == "FLOW_FRONT_FACE")
                                    {
                                        if (FlowFrontFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] != 0)   // The case where more than 2 wells locate in one cell is considered.
                                            DoubleNVAL = FlowFrontFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] + DoubleNVAL;
                                        FlowFrontFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] = DoubleNVAL;
                                    }
                                    else if (FormattedDESC == "FLOW_LOWER_FACE")
                                    {
                                        if (FlowLowerFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] != 0)   // The case where more than 2 wells locate in one cell is considered.
                                            DoubleNVAL = FlowLowerFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] + DoubleNVAL;
                                        FlowLowerFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] = DoubleNVAL;
                                    }
                                    /*else if (FormattedDESC == "RECHARGE")
                                    {
                                        if (RechargeRate[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] != 0)   // The case where more than 2 wells locate in one cell is considered.
                                            DoubleNVAL = FlowLowerFace[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] + DoubleNVAL;
                                        RechargeRate[(Column - 1), (Row - 1), (Layer - 1), CurrentTimeStep - 1] = DoubleNVAL;
                                    }
                                    else if (FormattedDESC == "WELLS")
                                    {
                                        if (WellFluxes.Exists(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (Column - 1), NumCellsY - (Row - 1) - 1, NumCellsZ - (Layer - 1) - 1 }))))
                                            WellFluxes[WellFluxes.FindLastIndex(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (Column - 1), NumCellsY - (Row - 1) - 1, NumCellsZ - (Layer - 1) - 1 })))].FlowRates[CurrentTimeStep - 1] = DoubleNVAL + WellFluxes[WellFluxes.FindLastIndex(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (Column - 1), NumCellsY - (Row - 1) - 1, NumCellsZ - (Layer - 1) - 1 })))].FlowRates[CurrentTimeStep - 1];
                                        else
                                        {
                                            WellFluxes.Add(new WellData
                                            {
                                                CellIndexes = Vector<double>.Build.Dense(new double[3] { (Column - 1), NumCellsY - (Row - 1) - 1, NumCellsZ - (Layer - 1) - 1 }), //Flipping Y and Z index
                                                FlowRates = new double[NumTimeSteps]
                                            });
                                            WellFluxes[WellFluxes.Count - 1].FlowRates[CurrentTimeStep - 1] = DoubleNVAL;
                                        }
                                    }*/
                                    else
                                        continue;
                                }
                            }
                            else if (NLIST == 0)
                                Console.WriteLine("[Info] No Values");
                            else
                                Console.WriteLine("[Warning] NLIST < 0");

                        }
                        else if (ITYPE == 3)
                        {
                            int[,] LayerIndex = new int[NumCellsX, NumCellsY]; //layer indicator
                            for (int r = 0; r < NumCellsY; r++)
                            {
                                for (int c = 0; c < NumCellsX; c++)
                                    LayerIndex[c, r] = BinaryReader.ReadInt32();
                            }

                            for (int r = 0; r < NumCellsY; r++)
                            {
                                for (int c = 0; c < NumCellsX; c++)
                                {
                                    if (FormattedDESC == "FLOW_RIGHT_FACE")
                                        FlowRightFace[c, r, LayerIndex[c, r], CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "FLOW_FRONT_FACE")
                                        FlowFrontFace[c, r, LayerIndex[c, r], CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "FLOW_LOWER_FACE")
                                        FlowLowerFace[c, r, LayerIndex[c, r], CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    /*else if (FormattedDESC == "RECHARGE")
                                        RechargeRate[c, r, LayerIndex[c, r], CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "WELLS")
                                    {
                                        if (WellFluxes.Exists(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (c - 1), NumCellsY - (r - 1) - 1, NumCellsZ - (LayerIndex[c, r] - 1) - 1 }))))
                                            WellFluxes[WellFluxes.FindLastIndex(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (c - 1), NumCellsY - (r - 1) - 1, NumCellsZ - (LayerIndex[c, r] - 1) - 1 })))].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        else
                                        {
                                            WellFluxes.Add(new WellData
                                            {
                                                CellIndexes = Vector<double>.Build.Dense(new double[3] { (c - 1), NumCellsY - (r - 1) - 1, NumCellsZ - (LayerIndex[c, r] - 1) - 1 }), //Flipping Y and Z index
                                                FlowRates = new double[NumTimeSteps]
                                            });
                                            WellFluxes[WellFluxes.Count - 1].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        }
                                    }*/
                                    else
                                        ReadReal(FileStream, RealValueBytes);
                                }
                            }
                        }
                        else if (ITYPE == 4)
                        {
                            for (int r = 0; r < NumCellsY; r++)
                            {
                                for (int c = 0; c < NumCellsX; c++)
                                {
                                    if (FormattedDESC == "FLOW_RIGHT_FACE")
                                        FlowRightFace[c, r, 0, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "FLOW_FRONT_FACE")
                                        FlowFrontFace[c, r, 0, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "FLOW_LOWER_FACE")
                                        FlowLowerFace[c, r, 0, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    /*else if (FormattedDESC == "RECHARGE")
                                        RechargeRate[c, r, 0, CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                    else if (FormattedDESC == "WELLS")
                                    {
                                        if (WellFluxes.Exists(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (c - 1), NumCellsY - (r - 1) - 1, NumCellsZ }))))
                                            WellFluxes[WellFluxes.FindLastIndex(x => x.CellIndexes.Equals(Vector<double>.Build.Dense(new double[3] { (c - 1), NumCellsY - (r - 1) - 1, NumCellsZ })))].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        else
                                        {
                                            WellFluxes.Add(new WellData
                                            {
                                                CellIndexes = Vector<double>.Build.Dense(new double[3] { (c - 1), NumCellsY - (r - 1) - 1, NumCellsZ}), //Flipping Y and Z index
                                                FlowRates = new double[NumTimeSteps]
                                            });
                                            WellFluxes[WellFluxes.Count - 1].FlowRates[CurrentTimeStep - 1] = ReadReal(FileStream, RealValueBytes);
                                        }
                                    }*/
                                    else
                                        ReadReal(FileStream, RealValueBytes);
                                }
                            }
                        }
                        else
                            Console.WriteLine("[WARNING} ITYPE value is not valid.");
                    }

                    NextChar = BinaryReader.PeekChar();
                    while (NextChar == 0) //removing padding number(0) between time steps
                    {
                        BinaryReader.ReadInt32();
                        NextChar = BinaryReader.PeekChar();
                    }
                    LoopCount++;
                } while (NextChar != -1);
            }
        }

        public double[,,,] GenerateRawFluxArray (String FluxFileName)
        {
            string[] Input = File.ReadAllLines(FluxFileName);
            char[] Delimiters = { ' ', '\t' };
            int RemainingFields;
            int TotalFields;
            string[] Nuggets;

            //Contain extra layer, row  or column - depends on the direction of flux
            double[,,,] RawFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];
            TotalFields = NumCellsY * NumCellsX;
            double[] Buffer = new double[TotalFields];

            //Filling 4-D array RawFluxArray
            //Reading from flux file. Each cell gets a flux arrow
            int i = 1; //Current Input row
            for (int TimeStepIdx = 0; TimeStepIdx < NumTimeSteps; TimeStepIdx++)
            {
                //Zero instead of one
                //for (int ZCellIndex = 0; ZCellIndex < NumCellsZ; ZCellIndex++)
                for (int ZCellIndex = NumCellsZ-1; ZCellIndex >= 0 ; ZCellIndex--)
                {
                    //Reading the flux file into the buffer
                    RemainingFields = TotalFields;
                    do
                    {
                        Nuggets = Input[i++].Split(Delimiters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string Nugget in Nuggets)
                        {
                            var Value = Convert.ToDouble(Nugget);
                            Buffer[TotalFields - RemainingFields] = Value;
                            RemainingFields--;
                        }
                    } while (RemainingFields > 0);

                    for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                    {
                        //Starting from zero instead of 1
                        for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                        {
                            RawFluxArray[XCellIndex, YCellIndex, ZCellIndex, TimeStepIdx]
                                = Buffer[YCellIndex * NumCellsX + XCellIndex];
                        }
                    }
                    i++; //Skip over header for next layer
                }
            }
            return RawFluxArray;
        }

        //Parse the flux file output by GW_Chart
        void SetXPlusFluxArray(double[,,,] RawFluxArray, out double[,,,] OutFluxArray)
        {
            OutFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];
            for (int TimeStepIdx = 0; TimeStepIdx < NumTimeSteps; TimeStepIdx++)
            {
                for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                {
                    for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                    {
                        for (int ZCellIndex = 0; ZCellIndex < NumCellsZ; ZCellIndex++)
                        {
                            //Need to FLIP YCellIndex: last row in file should correspond to YCellIndex = 0
                            //GW_Chart reports flow in +X direction as positive, so we are OK
                            OutFluxArray[XCellIndex, YCellIndex, ZCellIndex, TimeStepIdx] =
                            RawFluxArray[XCellIndex, NumCellsY - YCellIndex - 1, ZCellIndex, TimeStepIdx];
                        }
                    }
                }
            }
        }

        public double GetXPlusFlux(PositionData CellLocation, int TimeStepIdx)
        {
            int FAIdx = GetFluxArrayIndexFromTSIndex(TimeStepIdx);
            return XPlusFluxArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex, FAIdx];
        }

        public double GetXMinusFlux(PositionData CellLocation, int TimeStepIdx)
        {
            int FAIdx = GetFluxArrayIndexFromTSIndex(TimeStepIdx);
            if (CellLocation.XCellIndex > 0)
                return XPlusFluxArray[CellLocation.XCellIndex - 1, CellLocation.YCellIndex, CellLocation.ZCellIndex, FAIdx];
            else
                return 0;
        }

        void SetYMinusFluxArray(double[,,,] RawFluxArray, out double[,,,] OutFluxArray)
        {
            OutFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];
            for (int TimeStepIdx = 0; TimeStepIdx < NumTimeSteps; TimeStepIdx++)
            {
                for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                {
                    for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                    {
                        for (int ZCellIndex = 0; ZCellIndex < NumCellsZ; ZCellIndex++)
                        {
                            //Need to FLIP YCellIndex: last row in file should correspond to YCellIndex = 0
                            //Need to NEGATE values (GW_Chart reports flow in -Y direction as positive)
                            OutFluxArray[XCellIndex, YCellIndex, ZCellIndex, TimeStepIdx] =
                                -1.0 * RawFluxArray[XCellIndex, NumCellsY - YCellIndex - 1, ZCellIndex, TimeStepIdx];
                        }
                    }
                }
            }
        }

        public double GetYMinusFlux(PositionData CellLocation, int TimeStepIdx)
        {
            int FAIdx = GetFluxArrayIndexFromTSIndex(TimeStepIdx); 
            return YMinusFluxArray[CellLocation.XCellIndex, CellLocation.YCellIndex, CellLocation.ZCellIndex, FAIdx];
        }

        public double GetYPlusFlux(PositionData CellLocation, int TimeStepIdx)
        {
            int FAIdx = GetFluxArrayIndexFromTSIndex(TimeStepIdx);
            if (CellLocation.YCellIndex < NumCellsY - 1)
                return YMinusFluxArray[CellLocation.XCellIndex, CellLocation.YCellIndex + 1, CellLocation.ZCellIndex, FAIdx]; //Changed from [CellLocation.XcellIndex + 1,CellLocation.YCellIndex,..] to [CellLocation.XcellIndex,CellLocation.YcellIndex + 1, ...]
            else
                return 0;
        }

        //Normal version, for multiple layers
        void SetZFluxArray(double[,,,] RawFluxArray, out double[,,,] ZFluxArray)
        {
            ZFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ + 1, NumTimeSteps];
            for (int TimeStepIdx = 0; TimeStepIdx < NumTimeSteps; TimeStepIdx++)
            {
                for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                {
                    for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                    {
                        for (int ZCellIndex = 0; ZCellIndex < NumCellsZ; ZCellIndex++)
                        {
                            //Need to NEGATE values (GW_Chart reports flow in -Y direction as positive)
                            ZFluxArray[XCellIndex, YCellIndex, ZCellIndex, TimeStepIdx] =
                                -1.0 * RawFluxArray[XCellIndex, YCellIndex, ZCellIndex, TimeStepIdx];
                        }
                    }
                }
            }
        }


        void SetBlankXOrYFluxArray(out double[,,,] OutFluxArray)
        {
            OutFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];
        }

        //If there is a single layer, no .f4 file is generated and we just fill directly with zeros
        void SetBlankZFluxArray(out double[,,,] ZFluxArray)
        {
            ZFluxArray = new double[NumCellsX, NumCellsY, NumCellsZ + 1, NumTimeSteps];
        }

        public void CreateArrayFile(double[,,,] InputArray, string FileName)
        {
            using (StreamWriter sw = new(FileName + ".txt"))
            {
                for (int t =0; t < InputArray.GetLength(3); t++)
                {
                    for (int i = 0; i < InputArray.GetLength(2); i++)
                    {
                        sw.WriteLine("{0,6} {1,4} {2,14} {3,14} {4,16} {5,5} {6,5} {7,5}", 0, 0, 0, t+1, FileName, InputArray.GetLength(0), InputArray.GetLength(1), (i + 1));

                        for (int j = 0; j < InputArray.GetLength(1); j++)
                        {
                            sw.Write(" ");
                            for (int k = 0; k < InputArray.GetLength(0); k++)
                            {
                                if (InputArray[k, j, i, t] >= 0)
                                {
                                    sw.Write(String.Format("{0:0.000000E+000} ", InputArray[k, j, i, t]));
                                }
                                else
                                {
                                    sw.Write(String.Format("{0:0.00000E+000} ", InputArray[k, j, i, t]));
                                }
                            }
                            sw.Write("\n");
                        }
                    }
                }
            }
        }

    }
}
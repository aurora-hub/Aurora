﻿using System;
using System.IO;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Aurora
{
    partial class Simulation
    {
        public void GenerateHeadArray(String HeadOutputFile, out double[,,,] HeadArray)
        {
            {
                int RealValueBytes = 4;
                int DoubleCheck = 0;

                double[,,,] RawHeadArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];

                using (FileStream FileStreamForSingle = new FileStream(HeadOutputFile, FileMode.Open, FileAccess.Read))
                {

                    if (Path.GetExtension(HeadOutputFile) == ".bhd")
                    {
                        ReadBHDFile(out DoubleCheck, out RawHeadArray, FileStreamForSingle, RealValueBytes);
                    }
                    else if (Path.GetExtension(HeadOutputFile) == ".fhd")
                        Console.WriteLine("Please input binary head file (*.bhd) instead of formatted head file (*.fhd)");
                    else
                    {
                        Console.WriteLine("Extension of head file is not valid");
                        Environment.Exit(-2);
                    }
                    
                }
                if (DoubleCheck == 1)
                {
                    RealValueBytes = 8;

                    using (FileStream FlieStreamForDouble = new FileStream(HeadOutputFile, FileMode.Open, FileAccess.Read))
                        ReadBHDFile(out DoubleCheck, out RawHeadArray, FlieStreamForDouble, RealValueBytes);
                }

                HeadArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];

                for (int HeadArrayIdx = 0; HeadArrayIdx < HeadArrayIndexToStepIndex.Count; HeadArrayIdx++)
                {
                    for (int XCellIndex = 0; XCellIndex < NumCellsX; XCellIndex++)
                    {
                        for (int YCellIndex = 0; YCellIndex < NumCellsY; YCellIndex++)
                        {
                            for (int ZCellIndex = 0; ZCellIndex < NumCellsZ; ZCellIndex++)
                            {
                                //Need to FLIP YCellIndex: last row in file should correspond to YCellIndex = 0
                                //Need to FLIP ZCellIndex: last row in file should correspond to ZCellIndex = 0
                                HeadArray[XCellIndex, YCellIndex, ZCellIndex, HeadArrayIdx] =
                                RawHeadArray[XCellIndex, NumCellsY - YCellIndex - 1, NumCellsZ - ZCellIndex - 1, HeadArrayIdx];
                            }
                        }
                    }
                }
                SetHeadCellCornerArray();
            }
        }
        public void ReadBHDFile(out int DoubleCheck, out double[,,,] RawHeadArray, FileStream FileStream, int RealValueBytes)
        {
            int CurrentTimeStep = 1;

            RawHeadArray = new double[NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps];

            using (BinaryReader BinaryReader = new BinaryReader(FileStream))
            {
                List<string> Contents = new List<string>();
                List<string> ContentsForFirstTwoLoops = new List<string>()
                {"HEAD","DRAWDOWN", "SUBSIDENCE","COMPACTION","CRITICAL_HEAD",
                "HEAD_IN_HGU","NDSYS_COMPACTION","Z_DISPLACEMENT","D_CRITICAL_HEAD","LAYER_COMPACTION",
                "DSYS_COMPACTION","ND_CRITICAL_HEAD","LAYER_COMPACTION","SYSTM_COMPACTION","PRECONSOL_STRESS",
                "CHANGE_IN_PCSTRS","EFFECTIVE_STRESS","CHANGE_IN_EFF-ST","VOID_RATIO",
                "THICKNESS","CENTER_ELEVATION","GEOSTATIC_STRESS","CHANGE_IN_G-STRS"};

                int NextChar;
                DoubleCheck = 0;  //default: Real-value = Single
                int LoopCount = 1;

                HeadArrayIndexToStepIndex = new List<int>(); //initialize and populate global index array; different from flux approach

                do
                {
                    var Step = BinaryReader.ReadInt32(); //KSTP: time step number
                    var Period = BinaryReader.ReadInt32(); //KPER: stress period number
                    ReadReal(FileStream, RealValueBytes); //PERTIM: the time in the current stress period
                    ReadReal(FileStream, RealValueBytes); // TOTIM: the total elapsed time

                    int StepIdx = Period > 1 ? StressPeriodCumulativeSteps[Period - 2] + Step - 1 : Step - 1;
                    if (HeadArrayIndexToStepIndex.Count == 0)
                        HeadArrayIndexToStepIndex.Add(StepIdx);
                    else if (HeadArrayIndexToStepIndex[^1] < StepIdx)
                        HeadArrayIndexToStepIndex.Add(StepIdx);

                    string FormattedDESC = new string(BinaryReader.ReadChars(16)).Trim();
                    FormattedDESC = FormattedDESC.Replace(" ", "_");
                    if (!Contents.Contains(FormattedDESC))
                        Contents.Add(FormattedDESC);

                    BinaryReader.ReadInt32(); //NCOL: the number of columns in the array
                    BinaryReader.ReadInt32(); //NROW: the number of rows in the array
                    int ILAY = BinaryReader.ReadInt32(); //layer number

                    if (ILAY <=0 || ILAY > NumCellsZ)  //check if the data type is double precision
                    {
                        DoubleCheck = 1;
                        break;
                    }

                    if (LoopCount != 1 && Contents[0] == FormattedDESC && ILAY == 1)
                        CurrentTimeStep++;

                    if (LoopCount == 2 && !ContentsForFirstTwoLoops.Contains(FormattedDESC)) //check if the data type is double precision
                    {
                        DoubleCheck = 1;
                        break;
                    }

                    for (int j = 0; j < NumCellsY; j++)
                    {
                        for (int k = 0; k < NumCellsX; k++)
                            RawHeadArray[k, j, ILAY-1, CurrentTimeStep-1] = ReadReal(FileStream, RealValueBytes);
                    }

                    NextChar = BinaryReader.PeekChar();
                    while (NextChar == 0)
                    {
                        BinaryReader.ReadInt32();
                        NextChar = BinaryReader.PeekChar();
                    }
                    LoopCount++;
                } while (NextChar != -1);
            }
        }
        /*
         * The HeadCellCornerArray is indexed by TSIdx, not HAIdx, so it may contain duplicate fields.
         */
        void SetHeadCellCornerArray()
        {
            HeadCellCornerArray = new Vector<double>[NumCellsX + 1, NumCellsY + 1, NumCellsZ, NumTimeSteps];
            for (int TIdx = 0; TIdx < NumTimeSteps; TIdx++)
            {
                for (int XIdx = 0; XIdx <= NumCellsX; XIdx++)
                {
                    for (int YIdx = 0; YIdx <= NumCellsY; YIdx++)
                    {
                        for (int ZIdx = 0; ZIdx < NumCellsZ; ZIdx++)
                        {
                            double VertexZTYally = 0;
                            int NumCellsAveraged = 0;

                            //Average of neighbouring cells that are not out of bounds
                            for (int i = XIdx - 1; i <= XIdx; i++)
                                for (int j = YIdx - 1; j <= YIdx; j++)
                                    if (i >= 0 && i < NumCellsX && j >= 0 && j < NumCellsY)
                                    {
                                        int HAIdx = GetHeadArrayIndexFromTSIndex(TIdx);
                                        VertexZTYally += HeadArray[i, j, ZIdx, HAIdx];
                                        NumCellsAveraged++;
                                    }

                            HeadCellCornerArray[XIdx, YIdx, ZIdx, TIdx] = Vector<double>.Build.Dense(
                                new double[4] { XBoundArray[XIdx], YBoundArray[YIdx], VertexZTYally / NumCellsAveraged, TIdx });

                        }
                    }
                }    
            }
        }
    }
}

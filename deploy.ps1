﻿$ErrorActionPreference = 'Continue'

$relpath = "\Aurora\bin\Release\net6.0\publish\win-x64\"
$linuxrelpath = "\Aurora\bin\Release\net6.0\publish\linux-x64\"
$macosrelpath = "\Aurora\bin\Release\net6.0\publish\macos-x64\"

$currpath = (pwd).ToString()
echo $currpath

rm ($currpath+"\aurora.zip")
mkdir .\deploy-dir
mkdir .\deploy-dir\Visualizer

cp ($currpath + $relpath + "*") .\deploy-dir
cp ($currpath + $linuxrelpath + "Aurora") .\deploy-dir\aurora-linux-x64
cp ($currpath + $macosrelpath + "Aurora") .\deploy-dir\aurora-macos-x64
pdflatex.exe -interaction=nonstopmode ($currpath + "\Documentation\manual.tex")
cp ($currpath + "\Documentation\manual.pdf") .\deploy-dir\UserGuide.pdf 
echo ($currpath + "\Visualizer") 
cp -Path ($currpath + "\Visualizer\*.py") -Destination .\deploy-dir\Visualizer -Recurse
Copy-Item -Path ($currpath + "\Examples") -Destination .\deploy-dir\Examples -Recurse

Get-ChildItem ($currpath+"\deploy-dir") -recurse -include *.btc | remove-item
Get-ChildItem ($currpath+"\deploy-dir") -recurse -include *.pro | remove-item
Get-ChildItem ($currpath+"\deploy-dir") -recurse -include *.bat | remove-item
Get-ChildItem ($currpath+"\deploy-dir") -recurse -include *.archive | remove-item
Get-ChildItem ($currpath+"\deploy-dir") -recurse -include *-events.txt | remove-item
Get-ChildItem ($currpath+"\deploy-dir") -recurse -include *.btc | remove-item
Get-ChildItem ($currpath) manual.* | remove-item

Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($currpath+"\deploy-dir",$currpath+"\aurora.zip")

rmdir -r ($currpath+"\deploy-dir")
# -*- coding: utf-8 -*-
"""
VisBTC.py

Created on Wed May 18 02:27:17 2016

@author: Scott
"""
from numpy import fromfile, linspace
from scipy.stats import gaussian_kde
from pylab import gca, plot, show, subplots, title, xlabel, ylabel

directory = r"..\Examples\Simple Inclusion"
filename = "2.btc"

with open(directory + "\\" + filename,'r') as f:
    lines = f.readlines()

tp = lines[0].split("#")[0].split("\t")
#plottitle = "BTC at ax+by+cx=d, a=" + tp[0]+ " b=" + tp[1] + " c=" + tp[2]+ " d=" + tp[3]
plottitle = "Breakthrough"

points = [float(x.split()[0]) for x in lines[1:]]
points.sort()
print(points[-1], len(points))

kernel = gaussian_kde(points)
#times = linspace(points[0],points[-1],100)
times = linspace(0, points[-1], 500)
density = kernel(times)

fig, ax = subplots()
plot(times, density, lw=2, c='goldenrod')

#title(plottitle)
xlabel('Time')
ylabel('Relative concentration at plane')
show()
# -*- coding: utf-8 -*-
"""
VisPlumeMapFrame.py

Created on Wed Dec 5 2018

@author: Scott Hansen
"""
from numpy import flipud, mgrid, reshape, vstack, zeros
from scipy.stats import gaussian_kde
from matplotlib.image import imread
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d

def generate_frame(directory, filename, bounds):

    with open(directory + "\\" + filename,'r') as f:
        lines = f.readlines()

    alpha = 1.0

    secondsperyear = 365*24*60*60
    plottime = float(lines[0].split("#")[0].strip()) / secondsperyear
    plottitle = "Plume after " + "%0.4f" % plottime + " years"
    #plt.title(plottitle)

    numpts = len(lines)-1

    xs = zeros(numpts)
    zs = zeros(numpts)

    idx = 0
    for l in [line for line in lines[1:] if len(line) > 1]:
        pts = [float(x) for x in l.split("\t") if len(x) > 1]
        xs[idx] = pts[0]
        zs[idx] = pts[2]
        idx += 1

    xmin, xmax, zmin, zmax = bounds

    #Uncomment to draw colour map of inferred concentration
    """  X, Z = mgrid[xmin:xmax:100j, zmin:zmax:100j]
    positions = vstack([X.ravel(),Z.ravel()])
    values = vstack([xs, zs])
    kernel = gaussian_kde(values)
    H = reshape(kernel(positions).T, Z.shape) """

    fig, ax = plt.subplots()
    
    #ax.imshow(flipud(H), cmap=plt.cm.gist_earth_r, extent=[xmin, xmax, zmin, zmax], alpha=alpha, animated=True)
    ax.scatter(xs,zs, s = 1)
    ax.set_xlim([xmin,xmax])
    ax.set_ylim([zmin,zmax])
    #ax.text(0.5, 1.05, plottitle, transform=ax.transAxes, horizontalalignment='center')

if __name__ == "__main__":
    directory = r"..\Examples\Push Pull"
    filename = "1.pro"
    #bounds = [0, 55, -55, 0]
    bounds = [0., 60., -10, 0]

    generate_frame(directory, filename, bounds)
    
    plt.xlabel('x')
    plt.ylabel('z')
    plt.show()
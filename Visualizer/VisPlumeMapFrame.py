# -*- coding: utf-8 -*-
"""
VisPlumeMapFrame.py

Created on Wed Dec 5 2018

@author: Scott Hansen
"""
from numpy import array, cos, flipud, max, mgrid, min, pi, reshape, sin, vstack, zeros
from scipy.ndimage import rotate
from scipy.stats import gaussian_kde
from sys import argv
from types import SimpleNamespace
from matplotlib import patches
from matplotlib.image import imread
import matplotlib.pyplot as plt

def generate_frame(directory, filename, domaindata, figure=None):
    if figure == None:
        _, ax = plt.subplots()
    else: 
        ax, = figure.axes
    
    with open(directory + "\\" + filename,'r') as f:
        lines = f.readlines()

    angle = 0.0 if domaindata.angle == None else domaindata.angle / 360 * 2 * pi 

    if domaindata.backimagename == None:
        backimage = None
        alpha = 1.0
    else:
        rawimage = imread(directory + "\\" + domaindata.backimagename)
        if domaindata.angle is not None:
            backimage = rotate(rawimage, domaindata.angle, reshape=True, mode='constant')
        else:
            backimage = rawimage
        alpha = 1.0

    plottime = float(lines[0].split("#")[0].strip()) / domaindata.timeperdisptimeunit
    plottitle = "Plume after " + "%0.0f" % plottime + " " + domaindata.disptimeunit
    #plt.title(plottitle) 

    numpts = len(lines)-1

    xs = zeros(numpts)
    ys = zeros(numpts)

    idx = 0
    for l in lines[1:]:
        pts = [float(x) for x in l.strip().split("\t")]
        xs[idx] = pts[0]
        ys[idx] = pts[1]
        idx += 1

    ll = array((domaindata.lowerleft[0], domaindata.lowerleft[1]))
    lr = ll + array((domaindata.dimensions[0]*cos(angle), domaindata.dimensions[0]*sin(angle)))
    ul = ll + array((-domaindata.dimensions[1]*sin(angle), domaindata.dimensions[1]*cos(angle)))
    ur = ul + lr - ll
    corners = vstack([ll,lr,ul,ur])
    boundsrect = patches.Rectangle(ll,*domaindata.dimensions, angle=(360/2/pi*angle), ec='black', fill=False, linewidth=2)
       
    xmin, xmax, ymin, ymax = min(corners[:,0]), max(corners[:,0]), min(corners[:,1]), max(corners[:,1])
    Y, X = mgrid[ymin:ymax:100j, xmin:xmax:100j]
    positions = vstack([X.ravel(),Y.ravel()])
    values = vstack([xs, ys])
    kernel = gaussian_kde(values)
    H = reshape(kernel(positions).T, Y.shape)
    artist = ax.imshow(flipud(H), cmap=plt.cm.gist_earth_r, extent=[xmin, xmax, ymin, ymax], alpha=alpha, animated=True)
    
    title =  ax.text(0.5, 1.05, plottitle, transform=ax.transAxes, horizontalalignment='center')
    box = ax.add_patch(boundsrect)

    if backimage is None:
        return [artist, title, box]
    else:
        backartist = ax.imshow(backimage, extent=[xmin, xmax, ymin, ymax], alpha=0.3)
        return [backartist, artist, title, box]

if __name__ == "__main__":
    filename = str(argv[1]) + ".pro"
    directory = r"..\Examples\Rocky_Mountain_Arsenal"
    domaindata = {'lowerleft' : [1696.2, 3049.1], 
        'dimensions' : [4593, 5424], 
        'angle' : 0, 
        'backimagename' : "rma.png",
        'timeperdisptimeunit': 24*60*60,
        'disptimeunit' : 'days'}
    generate_frame(directory, filename, SimpleNamespace(**domaindata))

    '''
    directory = r"..\Examples\Chlorinated_Solvent_Decay"
    domaindata = {'lowerleft' : [0, -100], 
        'dimensions' : [500, 100], 
        'angle' : None, 
        'backimagename' : None,
        'timeperdisptimeunit': 24*60*60,
        'disptimeunit' : 'days'}
    generate_frame(directory, filename, SimpleNamespace(**domaindata))
    '''
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()